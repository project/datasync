<?php

/*
 * Job statuses
 */
define('DATASYNC_FAILED', 0);
define('DATASYNC_ADDED', 1);
define('DATASYNC_INITIALIZE', 2);
define('DATASYNC_ENQUEUE', 3);
define('DATASYNC_DEQUEUE', 4);
define('DATASYNC_FINISH', 5);
define('DATASYNC_DONE', 6);
/*
 * Job completeness
 */
define('DATASYNC_INCOMPLETE', 0);

/*
 * Queue actions
 */
define('DATASYNC_INSERT', 1);
define('DATASYNC_UPDATE', 2);
define('DATASYNC_DELETE', 3);

/*
 * Misc
 */
define('DATASYNC_UNSTARTED', 0);


