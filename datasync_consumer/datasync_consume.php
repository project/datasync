<?php

//die if we are not on the command line
if (!$argc) {
  exit();
}

//no need to chdir, we are already at drupal home

$args = $_SERVER['argv'];
$host = $args[1];

//bootstrap drupal
//hack in fake values for this global to trick drupal into bootstrapping
$GLOBALS['_SERVER']['SCRIPT_NAME'] = $host . '/cron.php';
require_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

//make sure there is not time limit
set_time_limit(0);
//up memory_limit
$mem_lim = 536870912; // 512M in bytes
ini_set('memory_limit', $mem_lim);

//fake login
global $user;
$tmp_user = $user;
$user = user_load(array('uid' => 1));

$start = time();


$valid_status = array(DATASYNC_INITIALIZE, DATASYNC_ENQUEUE, DATASYNC_DEQUEUE, DATASYNC_FINISH);
$consumers = datasync_get_consumers(TRUE, TRUE);
$consumer_names = array_keys($consumers);
$restart = ds_variable_get('datasync_fail_job_restart', 1);

exec("hostname", $chost);
trim($chost[0]);
$chost = $chost[0];

$max = 2147483647; //maxint
//get the max amount of time we'll wait before looking for more jobs
// (this will be the minimum max_interval);
foreach ($consumers as $c) {
  if ($c['generic']['max_interval'] < $max) {
    $max = $c['generic']['max_interval'];
  }
}


while (TRUE) {

$jobs = datasync_take_jobs(ds_variable_get('datasync_consumer_numjobs', 1), $valid_status, $consumer_names);

//if there are no jobs and no wait then start waiting at 1s
if (count($jobs) == 0 && $wait == 0) {
  $wait = 1;
}

foreach ($jobs as $job_id) {
  //if there are any jobs then there is no wait
  //TODO: these switch status calls are not in transactions. could result in jobs being processed multiple times (D6)
  $wait = 0;
  $job_info = datasync_get_job_info($job_id);
  switch($job_info->status) {
    case DATASYNC_INITIALIZE:
      $success = $consumers[$job_info->consumer]['generic']['initialize']($job_id);
      if ($success) {
        datasync_switch_status($job_id, DATASYNC_INITIALIZE, NULL, $job_info->started, time());
      }
      else {
        datasync_switch_status($job_id, DATASYNC_INITIALIZE, $restart ? DATASYNC_INITIALIZE : DATASYNC_INITIALIZE, $job_info->started, time());
      }
    break;
    case DATASYNC_ENQUEUE:
      $success = $consumers[$job_info->consumer]['generic']['enqueue']($job_id);
      if ($success) {
        datasync_switch_status($job_id, DATASYNC_ENQUEUE, NULL, $job_info->started, time());
      }
      else {
        datasync_switch_status($job_id, DATASYNC_ENQUEUE, $restart ? DATASYNC_INITIALIZE : DATASYNC_ENQUEUE, $job_info->started, time());
      }
    break;
    case DATASYNC_DEQUEUE:
      $success = datasync_consumer_dequeue($job_id, $consumers[$job_info->consumer]['generic']['dequeue']);
      if ($success) {
        datasync_switch_status($job_id, DATASYNC_DEQUEUE, NULL, $job_info->started, time());
      }
      else {
        datasync_switch_status($job_id, DATASYNC_DEQUEUE, $restart ? DATASYNC_INITIALIZE : DATASYNC_DEQUEUE, $job_info->started, time());
      }
    break;
    case DATASYNC_FINISH:
      $success = $consumers[$job_info->consumer]['generic']['finish']($job_id);
      if ($success) {
        datasync_switch_status($job_id, DATASYNC_FINISH, NULL, $job_info->started, time());
      }
      else {
        datasync_switch_status($job_id,  DATASYNC_FINISH,  $restart ? DATASYNC_INITIALIZE : DATASYNC_FINISH, $job_info->started, time());
      }
    break;
    default:
      watchdog("datasync", t("Invalid job status for generic datasync_consumer: %str", array('%str' => $job_info->status)), WATCHDOG_WARNING);
    break;
  }
}

//kill script every hour
if ((time()-$start) > 3600) {
  exit;
}
//kill script if we are almost out of memory
$tenM = 10485760; //10M in bytes
if (memory_get_usage() > ($mem_lim - $tenM)){
  exit;
}
//kill script if we are sent a signal
if (ds_variable_get("datasync_consumer_kill_$chost", 0)) {
  ds_variable_set("datasync_consumer_kill_$chost", 0);
  exit;
}

//exponential backoff
sleep($wait);
$wait = min(2 * $wait, $max);


}

$user = $tmp_user;


