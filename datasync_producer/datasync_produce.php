<?php

//die if we are not on the command line
if (!$argc) {
  exit();
}

//no need to chdir, we are already at drupal home

$args = $_SERVER['argv'];
$host = $args[1];

//bootstrap drupal
//hack in fake values for this global to trick drupal into bootstrapping
$GLOBALS['_SERVER']['SCRIPT_NAME'] = $host . '/cron.php';
require_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

//make sure there is not time limit
set_time_limit(0);
//up memory_limit
$mem_lim = 536870912; // 512M in bytes
ini_set('memory_limit', $mem_lim);

//fake login
global $user;
$tmp_user = $user;
$user = user_load(array('uid' => 1));

$start = time();

$producers = datasync_get_producers(TRUE, TRUE);

exec("hostname", $chost);
trim($chost[0]);
$chost = $chost[0];


$max = 2147483647; //maxint
//get the max amount of time we'll wait before looking for more jobs
// (this will be the minimum max_interval);
foreach ($producers as $p) {
  if ($p['generic']['max_interval'] < $max) {
    $max = $p['generic']['max_interval'];
  }
}


while (TRUE) {


datasync_run_advance($producers);
datasync_run_start($producers);
datasync_run_timeouts($producers);

//kill script every hour
if ((time()-$start) > 3600) {
  exit;
}
//kill script if we are almost out of memory
$tenM = 10485760; //10M in bytes
if (memory_get_usage() > ($mem_lim - $tenM)){
  exit;
}
if (ds_variable_get("datasync_producer_kill_$chost", 0)) {
  ds_variable_set("datasync_producer_kill_$chost", 0);
  exit;
}

/*
//exponential backoff
sleep($wait);
$wait = min(2 * $wait, $max);
*/
sleep($max);

}

$user = $tmp_user;


